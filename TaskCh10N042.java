﻿/**
 * В некоторых языках программирования (например, в Паскале) не преду-
 * смотрена операция возведения в степень. Написать рекурсивную функцию
 * для расчета степени n вещественного числа a (n — натуральное число).
 *
 * @author Dmitry
 */

 
import java.util.Scanner;


class TaskCh10N042 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    power pow = new power();

    System.out.print("Задай число = ");
    int a = in.nextInt();
    System.out.print("Задай степень = ");
    int n = in.nextInt();

    System.out.print("Число " + a + " в " + n + " степени = " + pow.pow(a, n));
  }
}

class power {
  int pow(int a,int n) {

    if (n > 0)
      return a*pow(a,n-1);
    else
      return 1;
  }
}