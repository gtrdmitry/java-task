﻿/**
 * Написать рекурсивную функцию, определяющую, является ли заданное натуральное
 * число простым (простым называется натуральное число, большее 1, не имеющее других
 * делителей, кроме единицы и самого себя).
 *
 * @author Dmitry
 */


import java.util.Scanner;

class TaskCh10N056 {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);
    Calculation calculation = new Calculation();

    System.out.print("Введите n - ");
    int n = in.nextInt();

    System.out.println(calculation.primeNumber(n, 2));
  }
}

class Calculation {
  boolean primeNumber(int n, int i) {
    if (n < 2) {
      return false;
    }
    else if (n == 2) {
      return true;
    }
    else if (n % i == 0) {
      return false;
    }
    else if (i < n / 2) {
      return primeNumber(n, i + 1);
    } else {
      return true;
    }
  }
}