﻿/**
 * Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh10N048 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.print("Задай длину массива - ");
    int n = in.nextInt();

    int[] array = new int[n];

    for (int i = 0; i < n; i++) {
      System.out.print("Задай a[" + i + "] элемент массива - ");
      array[i] = in.nextInt();
    }

    System.out.println("Максимальный элемент массива - " + calctulation.maxElement(array));
  }
}

class Calctulation {
  int maxIndex(int[] array, int start) {

    if (isMaxElement(array, array[start]))
      return start;
    return maxIndex(array, start + 1);
  }

  boolean isMaxElement(int[] array, int element) {

    for (int i = 0; i < array.length; i++) {
      if (array[i] > element)
        return false;
    }
    return true;
  }

  int maxElement(int[] array) {

    return array[maxIndex(array, array[0])];
  }
}