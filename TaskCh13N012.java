﻿/**
 * Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество,
 * адрес и дата поступления на работу (месяц, год). Напечатать фамилию,
 * имя, отчество и адрес сотрудников, которые на сегодняшний день проработали 
 * в фирме не менее трех лет. День месяца не учитывать (при совпадении 
 * месяца поступления и месяца сегодняшнего дня считать, что прошел полный год).
 * 
 * @author Dmitry
 */


import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012 {
  public static void main(String[] args) {

    List<Employee> employeesList = new ArrayList<>();
    Calculation calculation = new Calculation();

    employeesList.add(new Employee("Вадим", "Иванов", "Иванович", "Улица 1", 2010, 1));
    employeesList.add(new Employee("Иван", "Сидоров", "Улица 2", 2010, 1));
    employeesList.add(new Employee("Александр", "Белов", "Улица 3", 2017, 1));
    employeesList.add(new Employee("Петр", "Беляев", "Улица 4", 2014, 11));

    System.out.println("Список работников - ");
    for (Employee employees : employeesList) {
      System.out.println(employees);
    }

    System.out.println();
    System.out.println("Список работников, которые проработали более 3 лет - ");
    System.out.println(calculation.findEmployees(2017, 11, employeesList));

  }
}

class Employee {
  private String firstName;
  private String secondName;
  private String middleName;
  private String address;
  private int yearOfArrival;
  private int monthOfArrival;

  public Employee(String firstName, String secondName, String address, int yearOfArrival, int monthOfArrival) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.middleName = "";
    this.address = address;
    this.yearOfArrival = yearOfArrival;
    this.monthOfArrival = monthOfArrival;
  }

  public Employee(String firstName, String secondName, String middleName, String adress, int yearOfArrival, int monthOfArrival) {
    this(firstName, secondName, adress, yearOfArrival, monthOfArrival);
    this.middleName = middleName;
  }

  /* Не используем, т.к. работаем с конструктором

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setAdress(String adress) {
    this.address = adress;
  }

  public void setYearOfArrival(int yearOfArrival) {
    this.yearOfArrival = yearOfArrival;
  }

  public void setMonthOfArrival(int monthOfArrival) {
    this.monthOfArrival = monthOfArrival;
  }

  */

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getAddress() {
    return address;
  }

  public int getYearOfArrival() {
    return yearOfArrival;
  }

  public int getMonthOfArrival() {
    return monthOfArrival;
  }

  public int getYearsOfWork (int thisYear, int thisMonth) {
    int getYearsOfWork;
    getYearsOfWork = thisYear - getYearOfArrival();
    if (thisMonth < getMonthOfArrival())
      getYearsOfWork = getYearsOfWork - 1;
    return getYearsOfWork;
  }

  public String toString() {
    if (getMiddleName().isEmpty())
      return getFirstName() + " " + getSecondName() + " " + getAddress();
    else
      return getFirstName() + " " + getSecondName() + " " + getMiddleName() + " " + getAddress();
  }
}

class Calculation {
  public String findEmployees(int thisYear, int thisMonth, List<Employee> employees) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < employees.size(); i++) {
      String getInfo = employees.get(i).toString();
      if (3 <= employees.get(i).getYearsOfWork(thisYear, thisMonth)) {
        result.append(getInfo).append("\n");
      }
    }
    return result.toString();
  }
}