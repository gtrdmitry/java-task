/**
 * ���� ����� ����� h, m, s (0<=h<=23, 0<=m<=59, 0<=s<=59), ����������� ������ �������: 
 * h �����, m �����, s ������". ���������� ���� (� ��������) ����� ���������� 
 * ������� ������� � ������ ����� � � ��������� ������ �������.
 *
 * @author Dmitry
 */


import java.util.Scanner;


public class TaskCh02N039 {
    public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      float grade, z;
      System.out.printf("����� ���� = ");
      int h = in.nextInt();
      System.out.printf("����� ������ = ");
      int m = in.nextInt();
      System.out.printf("����� ������� = ");
      int s = in.nextInt();

      if ((h >= 0 && h <= 24) && (m >= 0 && m <= 60) && (s >= 0 && s <= 60)) {
        if (h >= 0 && h <= 12) {
          h = h % 12;
          z = h * 3600 + m * 60 + s;
          grade = 360 * z / 43200;
          System.out.println("���� ����� " + grade +" ��������");
        }
        else if (h > 12 && h <= 24) {
          h = h - 12;
          h = h % 12;
          z = h * 3600 + m * 60 * s;
          grade = 360 * z / 43200;
          System.out.println("���� ����� " + grade + " ��������");
        }
      }
        else System.out.println("������������ ��������");
    }
}