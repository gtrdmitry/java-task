﻿/**
 * Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи
 * 
 * @author Dmitry
 */
 

import java.util.Scanner;

public class TashCh10N047 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.print("Задай элемент - ");
    int k = in.nextInt();

    System.out.println(calctulation.Fibonnaci(k));
  }
}

class Calctulation {
  int Fibonnaci(int k) {

    if (k < 2) {
      return k;
    }
    else {
      return Fibonnaci(k - 1) + Fibonnaci(k - 2);
    }
  }
}