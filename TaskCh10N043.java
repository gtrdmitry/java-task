/**
 * Написать рекурсивную функцию:
 * а) вычисления суммы цифр натурального числа;
 * б) вычисления количества цифр натурального числа.
 *
 * @author Dmitry
 */


import java.util.Scanner;

class Main {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calculation objCalculation = new Calculation();

    System.out.print("Задай число = ");
    int n = in.nextInt();

    System.out.println("Сумма цифр = " + objCalculation.sumOfNumbers(n));
    System.out.println("Количество цифр = " + objCalculation.countOfNumbers(n));
  }
}



class Calculation {
  int sumOfNumbers(int n) {

    int result = 0;

    if (n == 0)
      return 0;
    else
      result = result + n % 10;
    result = result + sumOfNumbers(n / 10);
    return result;
  }

  int countOfNumbers(int n) {

    int count = 0;

    if (n == 0)
      return 0;
    else
      count = 1 + countOfNumbers(n/10);
    return count;
  }
}