﻿/**
 * Определить результат выполнения следующих рекурсивных процедур при n 5
 *
 * @author Dmitry
 */

 
import java.util.Scanner;

public class TaskCh10N051 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    calctulation.procedure1(5); //54321
    System.out.println();
    calctulation.procedure2(5); //12345
    System.out.println();
    calctulation.procedure3(5); //5432112345


  }
}

class Calctulation {
  void procedure1(int n) {

    if (n > 0) {
      System.out.print(n);
      procedure1(n - 1);
    }
  }

  void procedure2(int n) {

    if (n > 0) {
      procedure2(n - 1);
      System.out.print(n);
    }
  }

  void procedure3(int n) {

    if (n > 0) {
      System.out.print(n);
      procedure3(n - 1);
      System.out.print(n);
    }
  }
}