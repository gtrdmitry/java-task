/**
 * Дан двумерный массив.
 * а) Удалить из него k-ю строку.
 * б) Удалить из него s-й столбец.
 *
 * @author Dmitry
 */


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TaskCh12N234 {

  public static void main(String[] args) {

    deleteRowAndColumn deleteRowAndColumnObj = new deleteRowAndColumn();

    deleteRowAndColumnObj.deleteRow();
    deleteRowAndColumnObj.deleteColumn();
  }
}

class deleteRowAndColumn {
  void deleteRow() {

    Scanner in = new Scanner(System.in);

    Random random = new Random();

    List<List<Integer>> list = new ArrayList<>();

    System.out.print("Задай размерность матрицы - ");
    int n = in.nextInt();

    for (int i = 0; i < n; i++) {
      list.add(new ArrayList<>());
      for (int j = 0; j < n; j++) {
        list.get(i).add(random.nextInt(50));
      }
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf("%2d ", list.get(i).get(j));
      }
      System.out.println();
    }

    System.out.println("Какую строчку удалить ? ");
    int row = in.nextInt();

    list.remove(row-1);
    list.add(new ArrayList<>());

    for (int i = 0; i < n; i++) {
      list.get(list.size()-1).add(0);
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf("%2d ", list.get(i).get(j));
      }
      System.out.println();
    }
  }

  void deleteColumn() {

    Scanner in = new Scanner(System.in);

    Random random = new Random();

    List<List<Integer>> list = new ArrayList<>();

    System.out.print("Задай размерность матрицы - ");
    int n = in.nextInt();

    for (int i = 0; i < n; i++) {
      list.add(new ArrayList<>());
      for (int j = 0; j < n; j++) {
        list.get(i).add(random.nextInt(50));
      }
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf("%2d ", list.get(i).get(j));
      }
      System.out.println();
    }

    System.out.println("Какой столбец удалить ? ");
    int column = in.nextInt();

    for (int i = 0; i < n; i++) {
      list.get(i).remove(column-1);
      list.get(i).add(0);
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf("%2d ", list.get(i).get(j));
      }
      System.out.println();
    }
  }
}