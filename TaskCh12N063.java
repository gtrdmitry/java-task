/**
 * В двумерном массиве хранится информация о количестве учеников в том
 *или ином классе каждой параллели школы с первой по одиннадцатую (в первой строке — информация
 * о количестве учеников в первых классах, во второй — о вторых и т. д.).
 * В каждой параллели имеются 4 класса. Определить среднее количество учеников в классах каждой параллели.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TashCh12N063 {
  public void main() {

    Scanner in = new Scanner(System.in);

    int sum = 0;

    int[][] classes = {{10, 15, 12, 11},
                      {6, 10, 11, 26},
                      {8, 5, 16, 10},
                      {11, 6, 20, 11},
                      {24, 27, 13, 15},
                      {6, 21, 8, 21},
                      {12, 18, 6, 9},
                      {13, 12, 12, 18},
                      {18, 12, 18, 28},
                      {10, 9, 20, 10},
                      {11, 20, 12, 12}};

   for (int i = 0; i < classes.length; i++) {
     for (int j = 0; j < classes[i].length; j++) {
       sum += classes[i][j];
       if (j % 3 == 0 && j != 0) {
         System.out.println("Количество учеников в " + (i+1) + " классе - " + sum);
         sum = 0;
       }
     }
   }
  }
}