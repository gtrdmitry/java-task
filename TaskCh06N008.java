﻿/**
 * Дано число n. Из чисел 1, 4, 9, 16, 25, ... напечатать те, которые не превышают n.
 *
 * @author Dmitry
 */


import java.util.Scanner;
import java.lang.Math;


public class TaskCh06N008 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай количество элементов в массиве = ");
    int n = in.nextInt();
    int[] array = new int[n+1];

    array[0] = 0;
    for (int i = 0; i < n; i++) {
      array[i+1] = array[i] + 1;
    }

    for (int i = 0; i < n; i++) {
      array[i] = (int)Math.pow(array[i], 2);
    }

    for (int i = 0; i < n; i++) {
      if (n >= array[i]) System.out.printf("%d\n", array[i]);
    }

  }
}