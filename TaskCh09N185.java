﻿/**
 * Строка содержит арифметическое выражение, в котором используются
 * круглые скобки, в том числе вложенные. Проверить, правильно ли в нем рас-
 * ставлены скобки.
 * а) Ответом должны служить слова да или нет.
 * б) В случае неправильности расстановки скобок:
 * если имеются лишние правые (закрывающие) скобки, то выдать сооб-
 * щение с указанием позиции первой такой скобки;
 * если имеются лишние левые (открывающие) скобки, то выдать сообщение 
 * с указанием количества таких скобок.
 * Если скобки расставлены правильно, то сообщить об этом
 *
 * @author Dmitry
*/


import java.util.Scanner;


class TaskCh09N185 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    int countLeftBracket = 0;
    int countRightBracket = 0;

    System.out.print("Задай слово = ");
    StringBuilder strBuilder = new StringBuilder(in.nextLine());

    for (int i = 0; i < strBuilder.length(); i++) {
      if (strBuilder.charAt(i) == '(') {
        countLeftBracket++;
      }
      else if (strBuilder.charAt(i) == ')') {
        countRightBracket++;
      }
    }

    if (countLeftBracket == countRightBracket) {
      System.out.println("Да, скобки раставлены правильно");
    }
    else if (countLeftBracket > countRightBracket) {
      System.out.println("Количество лишних левых скобок = " + (countLeftBracket - countRightBracket));
    }
    else if (countRightBracket > countLeftBracket) {
      for (int i = strBuilder.length()-1; i>= 0; i--) {
        if (strBuilder.charAt(i) == ')') {
          System.out.println("Лишняя правая скобка находится по индексу = " + i);
          break;
        }
      }
    }
  }
}