﻿/**
 * Дано слово. Вывести на экран его k-й символ.
 *
 * @author Dmitry
 */


import java.util.Scanner;


class TaskCh09N015 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай слово = ");
    String str = in.nextLine();

    System.out.println("Какую по счету букву вывести ?");
    int k = in.nextInt();

    System.out.println(k + " по счету буква = " + str.charAt(k-1));
  }
}