import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OOP {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    List<GraduateGetJavaJob> graduatesGetJavaJob = new ArrayList<>();
    Candidate candidate = new GraduateGetJavaJob();
    List<SelfLearner> selfLearners = new ArrayList<>();
    SelfLearner selfLearner = new SelfLearner();
    Employer employer = new Employer();
    Check check = new Check();

    String firstName;
    String lastName;
    String middleName;
    String age;
    int countGraduate = 0;
    int countSelfLearners = 0;
    int n;

    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    System.out.println("It's a console application, dialogue between employer and candidate. Press enter to continue.");
    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    System.out.println("                                        Interview");
    System.out.print("---------------------------------------------------------------------------------" +
            "------------");
    in.nextLine();

    for (int i = 0; i < 10; i++) {
      if (i > 0) {
        System.out.println("---------------------------------------------------------------------------------" +
                "------------");
      }

      System.out.println("                                      " + (i + 1) + "/10 candidates");
      System.out.println("---------------------------------------------------------------------------------" +
              "------------");

      do {
        System.out.println("Are you getJavaJob graduate or self-learner ?");
        System.out.println("1. getJavaJob graduate");
        System.out.println("2. self-learner");
        System.out.print("> ");
        n = in.nextInt();
        if (n != 1 && n != 2) {
          System.out.println("---------------------------------------------------------------------------------" +
                  "------------");
          System.out.println("\t\t\t\t\t\t\t\t\t\tWrong number.");
          System.out.println("---------------------------------------------------------------------------------" +
                  "------------");
        }
      } while (n != 1 && n != 2);

      System.out.print(employer.sayHello());
      in.nextLine();
      in.nextLine();

      do {
        System.out.print(candidate.sayHello());
        firstName = in.nextLine();

        if (firstName.isEmpty() || !check.checkLetter(firstName)) {
          System.out.println("Wrong name.");
        }
      } while (firstName.isEmpty() || !check.checkLetter(firstName));

      do {
        System.out.print(candidate.tellLastName());
        lastName = in.nextLine();

        if (lastName.isEmpty() || !check.checkLetter(lastName)) {
          System.out.println("Wrong last name.");
        }
      } while (lastName.isEmpty() || !check.checkLetter(lastName));

      do {
        System.out.print(candidate.tellMiddleName());
        middleName = in.nextLine();

        if (middleName.isEmpty()) {
          middleName = "";
        }
        if (!check.checkLetter(middleName) && !middleName.isEmpty()) {
          System.out.println("Wrong middle name.");
        }
      } while (!check.checkLetter(middleName) && !middleName.isEmpty());


      do {
        System.out.print(candidate.tellAge());
        age = in.nextLine();

        if (age.isEmpty() || !check.checkDigit(age)) {
          System.out.println("Wrong age.");
        }
      } while (age.isEmpty() || !check.checkDigit(age));

      if (n == 1) {

        graduatesGetJavaJob.add(new GraduateGetJavaJob());
        graduatesGetJavaJob.get(countGraduate).setFirstName(firstName);
        graduatesGetJavaJob.get(countGraduate).setLastName(lastName);
        graduatesGetJavaJob.get(countGraduate).setMiddleName(middleName);
        graduatesGetJavaJob.get(countGraduate).setAge(Integer.parseInt(age));

        System.out.print(graduatesGetJavaJob.get(countGraduate).getFirstName() + ": " +
                graduatesGetJavaJob.get(countGraduate).describeSelf());
        in.nextLine();
        System.out.print(graduatesGetJavaJob.get(countGraduate).getFirstName() + ": " + candidate.askAboutWork());
        in.nextLine();
        System.out.print(employer.answerAboutWork());
        in.nextLine();
        System.out.print(employer.sayBye());
        in.nextLine();
        System.out.print(graduatesGetJavaJob.get(countGraduate).getFirstName() + ": " +
                graduatesGetJavaJob.get(countGraduate).sayBye());
        in.nextLine();
        countGraduate++;
      }

      else {

        selfLearners.add(new SelfLearner());
        selfLearners.get(countSelfLearners).setFirstName(firstName);
        selfLearners.get(countSelfLearners).setLastName(lastName);
        selfLearners.get(countSelfLearners).setMiddleName(middleName);
        selfLearners.get(countSelfLearners).setAge(Integer.parseInt(age));

        System.out.print(selfLearners.get(countSelfLearners).getFirstName() + ": " +
                selfLearners.get(countSelfLearners).describeSelf());
        in.nextLine();
        System.out.print(selfLearners.get(countSelfLearners).getFirstName() + ": " + selfLearner.askAboutWork());
        in.nextLine();
        System.out.print(employer.answerAboutWork());
        in.nextLine();
        System.out.print(employer.sayBye());
        in.nextLine();
        System.out.print(selfLearners.get(countSelfLearners).getFirstName() + ": " +
                selfLearners.get(countSelfLearners).sayBye());
        in.nextLine();
        countSelfLearners++;
      }
    }

    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    System.out.println("                                   getJavaJob graduates - ");
    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    for (GraduateGetJavaJob list : graduatesGetJavaJob) {
      System.out.println(list);
    }
    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    System.out.println("                                      self-learners - ");
    System.out.println("---------------------------------------------------------------------------------" +
            "------------");
    for (SelfLearner list : selfLearners) {
      System.out.println(list);
    }
  }
}


class Check {

  @SuppressWarnings("Duplicates")
  boolean checkLetter(String word) {

    boolean letterFound = false;
    boolean digitFound = false;
    boolean whiteSpaceFound = false;

    for (char ch : word.toCharArray()) {
      if (Character.isLetter(ch)) {
        letterFound = true;
      }
      else if (Character.isDigit(ch)) {
        digitFound = true;
      }
      else if (Character.isWhitespace(ch)) {
        whiteSpaceFound = true;
      }
    }
    return letterFound && !digitFound && !whiteSpaceFound;
  }

  @SuppressWarnings("Duplicates")
  boolean checkDigit(String word) {

    boolean letterFound = false;
    boolean digitFound = false;
    boolean whiteSpaceFound = false;

    for (char ch : word.toCharArray()) {
      if (Character.isLetter(ch)) {
        letterFound = true;
      }
      else if (Character.isDigit(ch)) {
        digitFound = true;
      }
      else if (Character.isWhitespace(ch)) {
        whiteSpaceFound = true;
      }
    }
    return !letterFound && digitFound && !whiteSpaceFound;
  }
}


/*
  Абстрактный класс Candidate с реализацией абстрактных методов в классах-наследниках. (абстракция)
  В абстрактном классе Candidate переменные являются private, т.е. изолированы от других классов (инкапсуляция).
  Чтобы получить данные этих переменных, используются сеттеры и геттеры.
  В классе Candidate создано несколько конструкторов с разными аргументами (перегрузка конструкторов).
  В классе Candidate создан метод sayHello, который перезаписан в классах-наследниках.
  Candidate candidate = new GraduateGetJavaJob();
  Создан объект класса Candidate, но с методами из GraduateGetJavaJob (полиморфизм).
 */


abstract class Candidate {
  private String firstName;
  private String lastName;
  private String middleName;
  private int age;

  public Candidate(String firstName, String lastName, int age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.middleName = "";
    this.age = age;
  }

  public Candidate(String firstName, String lastName, String middleName, int age) {
    this(firstName, lastName, age);
    this.middleName = middleName;
  }

  public Candidate() {

  }

  public Candidate(String firstName) {
    this.firstName = firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setAge(int age) {
    this.age = age;
  }


  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public int getAge() {
    return age;
  }

  public String askAboutWork() {
    return "Will I be able to work remotely in the future?";
  }

  public String sayHello() {
    return "Candidate: Hello!";
  }

  abstract String describeSelf();

  abstract String tellLastName();

  abstract String tellMiddleName();

  abstract String tellAge();

  @Override
  public String toString() {
    if (getMiddleName().isEmpty())
      return "Candidate{" +
              "firstName='" + getFirstName() + '\'' +
              ", lastName='" + getLastName() + '\'' +
              ", age=" + getAge() +
              '}';
    else
      return "Candidate{" +
              "firstName='" + getFirstName() + '\'' +
              ", lastName='" + getLastName() + '\'' +
              ", middleName='" + getMiddleName() + '\'' +
              ", age=" + getAge() +
              '}';
  }
}


/*
  Класс GraduateGetJavaJob является наследником класса Candidate (наследование).
  Класс GraduateGetJavaJob имплементирует интерфейс Speech.
  Класс GraduateGetJavaJob реализует абстрактные методы абстрактного класса Candidate.
 */


class GraduateGetJavaJob extends Candidate implements Speech {

  public GraduateGetJavaJob() {

  }

  @Override
  String describeSelf() {
    return "I passed successfully getJavaJob exams and code reviews.";
  }

  @Override
  public String sayHello() {
    return "Candidate: Hi! My name is <enter your name> - ";
  }

  @Override
  String tellLastName() {
    return "My last name is - ";
  }

  @Override
  String tellMiddleName() {
    return "My middle name is (if you don't have middle name, leave input empty and press enter) - ";
  }

  @Override
  String tellAge() {
    return "My age is - ";
  }

  @Override
  public String sayBye() {
    return "Thank you for your time. Have a nice day.";
  }
}


/*
  Класс SelfLearner является наследником класса Candidate (наследование).
  Класс SelfLearner имплементирует интерфейс Speech.
  Класс SelfLearner реализует абстрактные методы абстрактного класса Candidate.
 */


class SelfLearner extends Candidate implements Speech {

  @Override
  String describeSelf() {
    return "I have been learning Java by myself, nobody examined how through is my knowledge and how good is my code.";
  }

  @Override
  public String sayHello() {
    return "Candidate: Hi! My name is <enter your name> - ";
  }

  @Override
  String tellLastName() {
    return "My last name is - ";
  }

  @Override
  String tellMiddleName() {
    return "My middle name is (if you don't have middle name, leave input empty and press enter) - ";
  }

  @Override
  String tellAge() {
    return "My age is - ";
  }

  @Override
  public String sayBye() {
    return "I was glad to talk with you. Bye.";
  }
}


/*
  Класс Employer имплементирует интерфейс Speech.
 */


class Employer implements Speech {

  public String sayHello() {
    return "Employer: Hi! Welcome to the coolJavaProjects company. Introduce yourself and describe your java experience please.";
  }

  @Override
  public String sayBye() {
    return "Employer: Thanks for your answer.";
  }

  public String answerAboutWork() {
    return "Employer: Of course, this is not a problem, we let our employees work the way they like.";
  }
}


/*
  Интерфейс.
 */


interface Speech {

  String sayBye();
}