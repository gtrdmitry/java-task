﻿/**
 * Дано слово, состоящее из четного числа букв. Вывести на экран его первую
 * половину, не используя оператор цикла.
 *
 * @author Dmitry
*/

import java.util.Scanner;


class TaskCh09N022 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай слово = ");
    String str = in.nextLine();

    if (str.length() % 2 == 0) {
      System.out.println(str.substring(0, str.length()/2));
    }
    else System.out.println("Слово состоит из нечетного количества букв");
  }
}