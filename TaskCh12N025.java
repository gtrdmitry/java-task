﻿/**
 * Заполнить двумерный массив так, как представлено на рис.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh12N025 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calculation calculation = new Calculation();

    System.out.print("Введите размерность - ");
    int n = in.nextInt();
    int k = 1;

    int array[][] = new int[n][n];

    calculation.firstTable(array, k);
    System.out.println();
    calculation.secondTable(array, k);
    System.out.println();
    calculation.thirdTable(array, k);
    System.out.println();
    calculation.fourthTable(array, k);
    System.out.println();
    calculation.fifthTable(array, k);
    System.out.println();
    calculation.sixthTable(array, k);
    System.out.println();
    calculation.seventhTable(array, k);
    System.out.println();
    calculation.eightTable(array, k);
    System.out.println();
    calculation.ninthTable(array, k);
    System.out.println();
    calculation.tenthTable(array, k);
    System.out.println();
    calculation.eleventhTable(array, k);
    System.out.println();
    calculation.twelfthTable(array, k);
    System.out.println();
    calculation.thirteenthTable(array, k);
    System.out.println();
    calculation.fourteenthTable(array, k);
    System.out.println();
    calculation.fifteenthTable(array, k);
    System.out.println();
    calculation.sixteenthTable(array, k);
  }
}
class Calculation {
  void firstTable(int[][] firstTable, int k) {

    for (int i = 0; i < firstTable.length; i++) {
      for (int j = 0; j < firstTable[i].length; j++) {
        firstTable[i][j] = k;
        k++;
      }
    }
    for (int i = 0; i < firstTable.length; i++) {
      for (int j = 0; j < firstTable.length; j++) {
        System.out.printf("%3d ", firstTable[i][j]);
      }
      System.out.println();
    }
  }

  void secondTable(int[][] secondTable, int k) {

    for (int i = 0; i < secondTable.length; i++) {
      for (int j = 0; j < secondTable[i].length; j++) {
        secondTable[j][i] = k;
        k++;
      }
    }
    for (int i = 0; i < secondTable.length; i++) {
      for (int j = 0; j < secondTable.length; j++) {
        System.out.printf("%3d ", secondTable[i][j]);
      }
      System.out.println();
    }
  }

  void thirdTable(int[][] thirdTable, int k) {

    for (int i = 0; i < thirdTable.length; i++) {
      for (int j = thirdTable[i].length - 1; j >= 0; j--) {
        thirdTable[i][j] = k;
        k++;
      }
    }
    for (int i = 0; i < thirdTable.length; i++) {
      for (int j = 0; j < thirdTable.length; j++) {
        System.out.printf("%3d ", thirdTable[i][j]);
      }
      System.out.println();
    }
  }

  void fourthTable(int[][] fourthTable, int k) {

    for (int i = 0; i < fourthTable.length; i++) {
      for (int j = fourthTable.length -1 ; j >=0; j--) {
        fourthTable[j][i] = k;
        k++;
      }
    }
    for (int i = 0; i < fourthTable.length; i++) {
      for (int j = 0; j < fourthTable.length; j++) {
        System.out.printf("%3d ", fourthTable[i][j]);
      }
      System.out.println();
    }
  }

  void fifthTable(int[][] fifthTable, int k) {

    for (int i = 0; i < fifthTable.length; i++) {
      if (i % 2 == 0) {
        for (int j = 0; j < fifthTable[i].length; j++) {
          fifthTable[i][j] = k++;
        }
      }
      else {
        for (int j = fifthTable[i].length - 1; j >= 0; j--) {
          fifthTable[i][j] = k++;
        }
      }
    }

    for (int i = 0; i < fifthTable.length; i++) {
      for (int j = 0; j < fifthTable.length; j++) {
        System.out.printf("%3d ", fifthTable[i][j]);
      }
      System.out.println();
    }
  }

  void sixthTable(int[][] sixthTable, int k) {

    for (int i = 0; i < sixthTable.length; i++) {
      if (i % 2 == 0) {
        for (int j = 0; j < sixthTable.length; j++) {
          sixthTable[j][i] = k++;
        }
      }
      else {
        for (int j = sixthTable.length - 1; j >= 0; j--) {
          sixthTable[j][i] = k++;
        }
      }
    }

    for (int i = 0; i < sixthTable.length; i++) {
      for (int j = 0; j < sixthTable.length; j++) {
        System.out.printf("%3d ", sixthTable[i][j]);
      }
      System.out.println();
    }
  }

  void seventhTable(int[][] seventhTable, int k) {

    for (int i = seventhTable.length - 1; i >=0; i--) {
      for (int j = 0; j < seventhTable[i].length; j++) {
        seventhTable[i][j] = k;
        k++;
      }
    }
    for (int i = 0; i < seventhTable.length; i++) {
      for (int j = 0; j < seventhTable.length; j++) {
        System.out.printf("%3d ", seventhTable[i][j]);
      }
      System.out.println();
    }
  }

  void eightTable(int[][] eightTable, int k) {

    for (int i = eightTable.length - 1; i >= 0; i--) {
      for (int j = 0; j < eightTable.length; j++) {
        eightTable[j][i] = k;
        k++;
      }
    }

    for (int i = 0; i < eightTable.length; i++) {
      for (int j = 0; j < eightTable.length; j++) {
        System.out.printf("%3d ", eightTable[i][j]);
      }
      System.out.println();
    }
  }

  void ninthTable(int[][] ninthTable, int k) {

    for (int i = ninthTable.length - 1; i >= 0; i--) {
      for (int j = ninthTable[i].length - 1; j >= 0; j--) {
        ninthTable[i][j] = k;
        k++;
      }
    }

    for (int i = 0; i < ninthTable.length; i++) {
      for (int j = 0; j < ninthTable.length; j++) {
        System.out.printf("%3d ", ninthTable[i][j]);
      }
      System.out.println();
    }
  }

  void tenthTable(int[][] tenthTable, int k) {

    for (int i = tenthTable.length - 1; i >= 0; i--) {
      for (int j = tenthTable.length - 1; j >= 0; j--) {
        tenthTable[j][i] = k;
        k++;
      }
    }

    for (int i = 0; i < tenthTable.length; i++) {
      for (int j = 0; j < tenthTable.length; j++) {
        System.out.printf("%3d ", tenthTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void eleventhTable(int[][] eleventhTable, int k) {

    for (int i = eleventhTable.length - 1; i >= 0; i--) {
      if (i % 2 != 0) {
        for (int j = 0; j < eleventhTable.length; j++) {
          eleventhTable[i][j] = k++;
        }
      }
      else {
        for (int j = eleventhTable.length - 1; j >= 0; j--) {
          eleventhTable[i][j] = k++;
        }
      }
    }

    for (int i = 0; i < eleventhTable.length; i++) {
      for (int j = 0; j < eleventhTable.length; j++) {
        System.out.printf("%3d ", eleventhTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void twelfthTable(int[][] twelfthTable, int k) {

    for (int i = 0; i < twelfthTable.length; i++) {
      if (i % 2 == 0) {
        for (int j = twelfthTable.length - 1; j >= 0; j--) {
          twelfthTable[i][j] = k++;
        }
      }
      else {
        for (int j = 0; j < twelfthTable.length; j++) {
          twelfthTable[i][j] = k++;
        }
      }
    }

    for (int i = 0; i < twelfthTable.length; i++) {
      for (int j = 0; j < twelfthTable.length; j++) {
        System.out.printf("%3d ", twelfthTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void thirteenthTable(int[][] thirteenthTable, int k) {

    for (int i = thirteenthTable.length - 1; i >= 0; i--) {
      if (i % 2 != 0) {
        for (int j = 0; j < thirteenthTable.length; j++) {
          thirteenthTable[j][i] = k++;
        }
      }
      else {
        for (int j = thirteenthTable.length - 1; j >= 0; j--) {
          thirteenthTable[j][i] = k++;
        }
      }
    }

    for (int i = 0; i < thirteenthTable.length; i++) {
      for (int j = 0; j < thirteenthTable.length; j++) {
        System.out.printf("%3d ", thirteenthTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void fourteenthTable(int[][] fourteenthTable, int k) {

    for (int i = 0; i < fourteenthTable.length; i++) {
      if (i % 2 == 0) {
        for (int j = fourteenthTable.length - 1; j >= 0; j--) {
          fourteenthTable[j][i] = k++;
        }
      }
      else {
        for (int j = 0; j < fourteenthTable.length; j++) {
          fourteenthTable[j][i] = k++;
        }
      }
    }

    for (int i = 0; i < fourteenthTable.length; i++) {
      for (int j = 0; j < fourteenthTable.length; j++) {
        System.out.printf("%3d ", fourteenthTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void fifteenthTable(int[][] fifteenthTable, int k) {

    for (int i = fifteenthTable.length - 1; i >= 0; i--) {
      if (i % 2 != 0) {
        for (int j = fifteenthTable.length - 1; j >= 0; j--) {
          fifteenthTable[i][j] = k++;
        }
      }
      else {
        for (int j = 0; j < fifteenthTable.length; j++) {
          fifteenthTable[i][j] = k++;
        }
      }
    }

    for (int i = 0; i < fifteenthTable.length; i++) {
      for (int j = 0; j < fifteenthTable.length; j++) {
        System.out.printf("%3d ", fifteenthTable[i][j]);
      }
      System.out.println();
    }
  }

  @SuppressWarnings("Duplicates")
  void sixteenthTable(int[][] sixteenthTable, int k) {

    for (int i = sixteenthTable.length - 1; i >= 0; i--) {
      if (i % 2 != 0) {
        for (int j = sixteenthTable.length - 1; j >= 0; j--) {
          sixteenthTable[j][i] = k++;
        }
      }
      else {
        for (int j = 0; j < sixteenthTable.length; j++) {
          sixteenthTable[j][i] = k++;
        }
      }
    }

    for (int i = 0; i < sixteenthTable.length; i++) {
      for (int j = 0; j < sixteenthTable.length; j++) {
        System.out.printf("%3d ", sixteenthTable[i][j]);
      }
      System.out.println();
    }
  }
}