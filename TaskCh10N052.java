﻿/**
 * Написать рекурсивную процедуру для вывода на экран цифр натурального
 * числа в обратном порядке.
 * 
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh10N52 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.print("Задай число - ");
    int n = in.nextInt();

    System.out.println(calctulation.reverse(n));
  }
}

class Calctulation {
  int reverse(int n) {

    if (n < 10) {
      return n;
    }
    else {
      return Integer.parseInt(String.valueOf(n % 10) + reverse(n / 10));
    }
  }
}