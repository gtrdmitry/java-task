﻿/**
 * Написать рекурсивную функцию для вычисления значения так называемой
 * функции Аккермана для неотрицательных чисел n и m.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh10N050 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.print("Задай n - ");
    int n = in.nextInt();
    System.out.print("Задай m - ");
    int m = in.nextInt();

    System.out.println(calctulation.akkerman(n, m));
  }
}

class Calctulation {
  int akkerman(int n, int m) {

    if (n == 0) {
      return m + 1;
    }
    else if (n != 0 && m == 0) {
      return akkerman(n - 1, 1);
    }
    else {
      return akkerman(n - 1, akkerman(n, m - 1));
    }
  }
}