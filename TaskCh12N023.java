﻿/**
 * Заполнить двумерный массив размером 7 7 так, как показано на рис.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh12N023 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calculation calculation = new Calculation();

    System.out.print("Введите размерность - ");
    int n = in.nextInt();

    int array[][] = new int[n][n];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = 0;
      }
    }

    calculation.firstTable(array);
    calculation.secondTable(array);
    calculation.thirdTable(array);

  }
}
class Calculation {
  void firstTable(int[][] firstTable) {

    for (int i = 0; i < firstTable.length; i++) {
      for (int j = 0; j < firstTable.length; j++) {
        if (i == j) {
          firstTable[i][j] = 1;
        } else if (i + 1 == firstTable.length - j) {
          firstTable[i][j] = 1;
        }
      }
    }
    for (int i = 0; i < firstTable.length; i++) {
      for (int j = 0; j < firstTable.length; j++) {
        System.out.printf("%2d ", firstTable[i][j]);
      }
      System.out.println();
    }
  }

  void secondTable(int[][] secondTable) {

    System.out.println();

    for (int i = 0; i < secondTable.length; i++) {
      for (int j = 0; j < secondTable.length; j++) {
        if (i == j) {
          secondTable[i][j] = 1;
        } else if (i + 1 == secondTable.length - j) {
          secondTable[i][j] = 1;
        } else if (j == (secondTable.length - 1) / 2) {
          secondTable[i][j] = 1;
        } else if (i == (secondTable.length - 1) / 2) {
          secondTable[i][j] = 1;
        }
      }
    }
    for (int i = 0; i < secondTable.length; i++) {
      for (int j = 0; j < secondTable.length; j++) {
        System.out.printf("%2d ", secondTable[i][j]);
      }
      System.out.println();
    }
  }

  void thirdTable(int[][] thirdTable) {

    System.out.println();

    for (int i = 0; i <= thirdTable.length / 2; i++) {
      for (int j = i; j < thirdTable[i].length - i; j++) {
        thirdTable[i][j] = thirdTable[thirdTable.length - i - 1][j] = 1;
      }
    }
    for (int i = 0; i < thirdTable.length; i++) {
      for (int j = 0; j < thirdTable.length; j++) {
        thirdTable[thirdTable.length / 2][j] = 0;
        thirdTable[thirdTable.length / 2][thirdTable.length / 2] = 1;
        if (i == 0 || i == thirdTable.length - 1) {
          thirdTable[i][j] = 1;
        }
      }
    }
    for (int i = 0; i < thirdTable.length; i++) {
      for (int j = 0; j < thirdTable.length; j++) {
        System.out.printf("%2d ", thirdTable[i][j]);
      }
      System.out.println();
    }
  }
}