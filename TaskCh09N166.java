﻿/**
 * Дано предложение. Поменять местами его первое и последнее слово.
 *
 * @author Dmitry
*/


import java.util.Scanner;


class TaskCh09N166 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    String firstWord;
    String secondWord;

    System.out.print("Задай предложение = ");
    String str = in.nextLine();
    String[] strParts = str.split(" ");

    firstWord = strParts[0];
    secondWord = strParts[strParts.length-1];
    strParts[0] = secondWord;
    strParts[strParts.length-1] = firstWord;

    for (int i = 0; i < strParts.length; i++) {
      System.out.print(strParts[i] + " ");
    }
  }
}