﻿/**
 * Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же букву?
 *
 * @author Dmitry
 */


import java.util.Scanner;


class TaskCh09N017 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай слово = ");
    String str = in.nextLine();

    if (str.charAt(0) == str.charAt(str.length()-1)) System.out.println("Первая и последняя буквы одинаковы");
    else System.out.println("Первая и последняя буквы неодинаковы");
  }
}