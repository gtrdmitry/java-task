/**
 * В области 12 районов. Известны количество жителей (в тысячах человек)
 * и площадь (в км2) каждого района. Определить среднюю плотность населения
 * по области в целом.
 *
 * @author Dmitry
 */


public class TaskCh05N064 {

  public static void main(String[] args) {

    Calculation javatask = new Calculation();

    int[][] population = {{1000, 150},
                         {6000, 100},
                         {8000, 450},
                         {1100, 180},
                         {2400, 270},
                         {600, 80},
                         {1200, 180},
                         {1300, 120},
                         {1800, 120},
                         {1000, 90},
                         {1100, 200},
                         {1000, 150}};

    javatask.main(population);

  }
}

class Calculation {
  public void main(int[][] population) {

    int citizen = 0;
    int square = 0;

    for (int i = 0; i < population.length; i++) {
      citizen += population[i][0];
      square += population[i][1];
    }

    System.out.print("Средняя плотность населения - " + citizen/square);
  }
}