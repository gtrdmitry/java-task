﻿/**
 * Составить программу, которая печатает заданное слово, начиная с последней буквы.
 *
 * @author Dmitry
*/

import java.util.Scanner;


class TaskCh09N042 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай слово = ");
    StringBuilder strBuilder = new StringBuilder(in.nextLine());

    for (int i = strBuilder.length()-1; i >= 0; i--) System.out.print(strBuilder.charAt(i));
  }
}