/**
 * Заполнить массив размером 6x6 так, как показано на рис. 12.2.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh12N024 {
  public void main() {

    Scanner in = new Scanner(System.in);

    System.out.print("Введите размерность: ");
    int n = in.nextInt();

    int array[][] = new int[n][n];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = 1;
      }
    }

    System.out.println("Массив А:");
    for (int i = 1; i < array.length; i++) {
      for (int j = 1; j < array.length; j++) {
        array[i][j] = array[i][j - 1] + array[i - 1][j];
      }
    }
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }

    System.out.println("Массив B:");
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = ((i + j) % n) + 1;
      }
    }

    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }
}