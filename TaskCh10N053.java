﻿/**
 * Написать рекурсивную процедуру для ввода с клавиатуры последовательности 
 * чисел и вывода ее на экран в обратном порядке (окончание последовательности — при вводе нуля).
 * 
 * @author Dmitry
 */


import java.util.Scanner;

public class TaskCh10N053 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calctulation calctulation = new Calctulation();

    String[] partsString;
    int count = 0;

    System.out.print("Введь последовательность чисел - ");
    String stringOfNumbers = in.nextLine();
    partsString = stringOfNumbers.split(" ");

    int[] partsInt = new int[partsString.length];
    for (int i = 0; i < partsString.length; i++) {
      partsInt[i] = Integer.parseInt(partsString[i]);
    }

    for (int i = 0; i < partsInt.length; i++) {
      if (partsInt[i] == 0) {
        break;
      } else {
        count++;
      }
    }

    int[] seqeunceArray = new int[count];
    for (int i = 0; i < count; i++) {
      seqeunceArray[i] = partsInt[i];
    }

    for (int i = 0; i < seqeunceArray.length; i++) {
      System.out.println(seqeunceArray[i]);
    }

    System.out.println("Reverse");
    calctulation.reverse(seqeunceArray, seqeunceArray.length, 0);
  }
}

class Calctulation {
  void reverse(int[] array, int len, int i) {

    if (i < len) {
      System.out.println(array[len - i - 1]);
      i++;
      reverse(array, len, i);
    }
  }
}