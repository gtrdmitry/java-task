﻿/**
 * Написать рекурсивную функцию для вычисления факториала натурального числа n.
 *
 * @author Dmitry
*/


import java.util.Scanner;


class TaskCh10N041 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    factorial f = new factorial();

    System.out.print("Задай n для факториала = ");
    int n = in.nextInt();

    System.out.print("Факториал из " + n + " равен = " + f.fact(n));
  }
}

class factorial {
  int fact(int n) {

    if (n == 1)
      return 1;
    else
      return fact(n-1) * n;
  }
}