﻿/**
 * Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему
 * курсу (значение курса вводится с клавиатуры).
 *
 * @author Dmitry
 */


import java.util.Scanner;


public class TaskCh05N010 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
    int i;

    System.out.print("Задай значение курса = ");
    float kurs = in.nextFloat();

    a[0] = 1;
    for (i = 1; i < 20; i++) {
      a[i] = a[i - 1] + 1;
    }

    for (i = 0; i < 20; i++) {
      System.out.println(a[i] + " dollars = " + (a[i] * kurs) + " rub");
    }
  }
}