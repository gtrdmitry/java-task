﻿/**
 * Написать рекурсивную процедуру перевода натурального числа из десятичной
 * системы счисления в N-ричную. Значение N в основной программе вводится с клавиатуры (2 N 16).
 *
 * @author Dmitry
 */


import java.util.Scanner;

class TaskCh10N055 {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);
    Calculation calculation = new Calculation();

    System.out.print("Введите n - ");
    int n = in.nextInt();

    System.out.println("Введите систему счисления - ");
    int numeralSystem = in.nextInt();

    switch (numeralSystem) {
      case 2 :
        calculation.toBin(n);
        break;

      case 8 :
        calculation.toOct(n);
        break;

      case 16 :
        calculation.toHex(n);
        break;

      default :
        System.out.println("Неправильная система счисления");
    }
  }
}

class Calculation {
  void toBin(int n) {
    if (n > 0) {
      toBin(n / 2);
      System.out.print(n % 2 + " ");
    }
  }

  void toOct(int n) {
    if (n > 0) {
      toOct(n / 8);
      System.out.print(n % 8 + " ");
    }
  }

  void toHex(int n) {
    if (n > 0) {
      toHex(n / 16);
      System.out.printf("%X", n % 16);
    }
  }
}