﻿/**
 * Дано слово. Поменять местами первую из букв а и последнюю из букв о.
 * Учесть возможность того, что таких букв в слове может не быть.
 *
 * @author Dmitry
*/


import java.util.Scanner;


class TaskCh09N107 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    int letterA = 0;
    int letterO = 0;

    System.out.print("Задай слово = ");
    StringBuilder strBuilder = new StringBuilder(in.nextLine());

    for (int i = 0; i < strBuilder.length(); i++) {
      if (strBuilder.charAt(i) == 'a') {
        letterA = i;
        break;
      }
    }
    for (int i = strBuilder.length()-1; i >= 0; i--) {
      if (strBuilder.charAt(i) == 'o') {
        letterO = i;
        break;
      }
    }

    if (letterA != 0 || letterO != 0) {
      strBuilder.setCharAt(letterA, 'o');
      strBuilder.setCharAt(letterO, 'a');
      System.out.println("Поменять местами первую 'а' с последней 'о' = " + strBuilder);
    }
    else System.out.println("Таких букв нет");
  }
}