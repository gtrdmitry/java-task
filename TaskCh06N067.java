/**
 * Составить программу, которая ведет учет очков, набранных каждой командой
 * при игре в баскетбол. Количество очков, полученных командами в ходе игры,
 * может быть равно 1, 2 или 3. После любого изменения счет выводить на
 * экран. После окончания игры выдать итоговое сообщение и указать номер
 * команды-победительницы. Окончание игры условно моделировать вводом
 * количества очков, равного нулю.
 *
 * @author Dmitry
 */


import java.util.Scanner;

public class Main {
  public static void main (String[] args) {

    Game gameObj = new Game();

    gameObj.getTeamNames();
    gameObj.play();
    gameObj.getResult();
  }
}

class Game {
  private int scoreTeam1;
  private String TeamOne;
  private int scoreTeam2;
  private String TeamTwo;

  void getTeamNames() {

    Scanner in = new Scanner(System.in);
    System.out.println("Enter team one name");
    TeamOne = in.nextLine();
    System.out.println("Enter team two name");
    TeamTwo = in.nextLine();
  }

  void play() {

    Scanner in = new Scanner(System.in);

    int n;
    int score;

    do {
      System.out.println("Enter team to score (1 or 2 or 0 to finish game)");
      n = in.nextInt();
       if (n == 1) {
        System.out.println("Enter score (1 or 2 or 3)");
        score = in.nextInt();
        if (score == 1 || score == 2 || score == 3) {
          scoreTeam1 +=score;
          System.out.println(TeamOne + " score - " + scoreTeam1);
          System.out.println(TeamTwo + " score - " + scoreTeam2);
        }
        else {
          System.out.println("Wrong value. Re-enter.");
        }
      }
      else if (n == 2) {
        System.out.println("Enter score (1 or 2 or 3)");
        score = in.nextInt();
        if (score == 1 || score == 2 || score == 3) {
          scoreTeam2 += score;
          System.out.println(TeamOne + " score - " + scoreTeam1);
          System.out.println(TeamTwo + " score - " + scoreTeam2);
        }
        else {
          System.out.println("Wrong value. Re-enter.");
        }
      }
    } while (n != 0);
  }

  void getResult() {

    if (scoreTeam1 > scoreTeam2)
      System.out.println(TeamOne + " score - " + scoreTeam1 + "\n" + TeamTwo + " score - " + scoreTeam2 + "\nWinner - " + TeamOne);
    else if (scoreTeam2 > scoreTeam1)
      System.out.println(TeamOne + " score - " + scoreTeam1 + "\n" + TeamTwo + " score - " + scoreTeam2 + "\nWinner - " + TeamTwo);
    else
      System.out.println(TeamOne + " score - " + scoreTeam1 + "\n" + TeamTwo + " score - " + scoreTeam2 + "\nResult - draw");
  }
}