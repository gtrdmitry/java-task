﻿/**
 * Записать условие, которое является истинным, когда:
 * а)каждое из чисел X и Y нечетное;
 * б)только одно из чисел X и Y меньше 20;
 * в)хотя бы одно из чисел X и Y равно нулю;
 * г)каждое из чисел X, Y, Z отрицательное;
 * д)только одно из чисел X, Y и Z кратно пяти;
 * е)хотя бы одно из чисел X, Y, Z больше 100.
 *
 * @author Dmitry
 */
 
 
 class TaskCh03N029 {

  public static boolean termA(int x, int y) {
    return x % 2 != 0 && y % 2 != 0;
  }

  public static boolean termB(int x, int y) {
    return x <= 2 && y > 2 || x > 2 && y <= 2;
  }

  public static boolean termV(int x, int y) {
    return x == 0 || y == 0;
  }

  public static boolean termG(int x, int y, int z) {
    return x < 0 && y < 0 && z < 0;
  }

  public static boolean termD(int x, int y, int z) {
    return (x % 3 == 0 && y % 3 != 0 && z % 3 != 0) || (x % 3 != 0 && y % 3 == 0 && z % 3 != 0) || (x % 3 != 0 && y % 3 != 0 && z % 3 == 0);
  }

  public static boolean termE(int x, int y, int z) {
    return x > 100 || y > 100 || z > 100;
  }
}