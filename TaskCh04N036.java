﻿/**
 * Работа светофора для пешеходов запрограммирована следующим образом: в
 * начале каждого часа в течение трех минут горит зеленый сигнал, затем в те-
 * чение двух минут — красный, в течение трех минут — опять зеленый и т. д.
 * Дано вещественное число t, означающее время в минутах, прошедшее с нача-
 * ла очередного часа. Определить, сигнал какого цвета горит для пешеходов в
 * этот момент.
 *
 * @author Dmitry
 */


import java.util.Scanner;


class TaskCh04N036 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Введите время = ");
    int minute = in.nextInt();
    int primaryIndex = minute;
    int altIndex = minute;

    if (altIndex % 5 != 0) {
      while (altIndex % 5 != 0) {
        altIndex = altIndex + 1;
      }
    }

    if (altIndex % 5 == 0) {
      if (primaryIndex == altIndex - 1 || primaryIndex == altIndex - 2) {
        System.out.println(primaryIndex + " - красный свет");
      } else if (primaryIndex == altIndex + 1 || primaryIndex == altIndex + 2) {
        System.out.println(primaryIndex + " - зеленый свет");
      } else if (altIndex % 5 == 0) {
        System.out.println(primaryIndex + " - зеленый свет");
      }
    }
  }
}