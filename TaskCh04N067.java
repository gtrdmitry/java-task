﻿/**
 * Дано целое число k (1 k 365). Определить, каким будет k-й день года: выходным
 * (суббота и воскресенье) или рабочим, если 1 января — понедельник.
 *
 * @author Dmitry
 */


import java.util.Scanner;


class TaskCh04N067 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Введите день = ");
    int k = in.nextInt();

    if (k > 0 && k < 366) {
      if (k % 7 >= 1 && k % 7 <= 5) {
        System.out.println("Рабочий день");
      }
      else System.out.println("Выходной");
    }
    else System.out.println("Неправильный день");
  }
}