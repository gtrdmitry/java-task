﻿/**
 * Заполнить двумерный массив размером 5 5 так, как представлено на рис.
 *
 * @author Dmitry
 */

 
import java.util.Scanner;

class TaskCh12N028 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.println("Введите размерность массива: ");
    int n = in.nextInt();

    int[][] array = new int[n][n];
    int row = 0;
    int column = 0;
    int indexColumn = 1;
    int indexRow = 0;
    int changeDirection = 0;
    int check = array.length;

    for (int i = 0; i < array.length*array.length ; i++) {
      array[row][column] = i + 1;
      if (--check == 0) {
        check = array.length * (changeDirection % 2) + array.length * ((changeDirection + 1) % 2) - (changeDirection / 2 - 1) - 2;
        int cell = indexColumn;
        indexColumn = -indexRow;
        indexRow = cell;
        changeDirection++;
      }
      column += indexColumn;
      row += indexRow;
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf("%5d ", array[i][j]);
      }
      System.out.println();
    }
  }
}