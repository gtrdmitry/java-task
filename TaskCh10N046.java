﻿/**
 * Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию:
 * а) нахождения n-го члена прогрессии;
 * б) нахождения суммы n первых членов прогрессии
 *
 * @author Dmitry
 */

 
import java.util.Scanner;

public class TaskCh10N046 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.print("Задай первый член геометирческой прогрессии - ");
    int first = in.nextInt();
    System.out.print("Задай разность прогрессии - ");
    int difference = in.nextInt();
    System.out.print("Задай номер члена прогрессии - ");
    int n = in.nextInt();

    System.out.println(n + " член данной прогрессии - " + calctulation.geometricalProgressionElement(first, difference, n));
    System.out.println("Сумма членов заданной прогрессии - " + calctulation.geometricalProgressionSum(first, difference, n));
  }
}

class Calctulation {
  int geometricalProgressionElement(int first, int difference, int n) {

    if (n == 1) {
      return first;
    }
    else {
      return geometricalProgressionElement(first * difference, difference, n - 1);
    }
  }

  int geometricalProgressionSum(int first, int difference, int n) {

    if (n == 1) {
      return first;
    }
    else {
      return geometricalProgressionSum(first, difference, n - 1) + geometricalProgressionElement(first, difference, n);
    }
  }
}