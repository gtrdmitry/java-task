﻿/**
 * Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
 * а) n-го члена прогрессии;
 * б) суммы n первых членов прогрессии.
 *
 * @author Dmitry
 */

 
import java.util.Scanner;

public class TaskCh10N045 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    Calctulation calctulation = new Calctulation();

    System.out.println("Задай первый член арифметической прогрессии - ");
    int first = in.nextInt();
    System.out.println("Задай разность прогрессии - ");
    int difference = in.nextInt();
    System.out.print("Задай номер члена прогрессии - ");
    int n = in.nextInt();

    System.out.println(n + " член данной прогрессии - " + calctulation.arithmeticProgressionElement(first, difference, n));
    System.out.println("Сумма членов заданной прогрессии - " + calctulation.arithmeticProgressionSum(first, difference, n));
  }
}

class Calctulation {
  int arithmeticProgressionElement(int first, int difference, int n) {

    if (n == 1) {
      return first;
    }
    else {
      return arithmeticProgressionElement(first + difference, difference, n - 1);
    }
  }

  int arithmeticProgressionSum(int first, int difference, int n) {

    if (n == 1) {
      return first;
    }
    else {
      return arithmeticProgressionSum(first, difference, n - 1) + arithmeticProgressionElement(first, difference, n);
    }
  }
}