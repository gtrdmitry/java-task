﻿/**
 * Дано натуральное число.
 * а) Верно ли, что оно заканчивается четной цифрой?
 * б) Верно ли, что оно заканчивается нечетной цифрой?
 *
 * Примечание
 * В обеих задачах составное условие не использовать.
 *
 * @author Dmitry
 */


import java.util.Scanner;


class TaskCh04N033 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.println("Задай число = ");
    int number = in.nextInt();

    number = number % 10;
    number = number % 2;

    System.out.println(number);
    System.out.println("Ввывод на экран '1' означает нечетность последней цифры заданного числа");
    System.out.println("Ввывод на экран '0' означает четность последней цифры заданного числа");
  }
}