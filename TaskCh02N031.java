﻿/**
 * В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному при 
 * этом двузначному числу справа приписали вторую цифру числа x, то получилось 
 * число n. 
 * По заданному n найти число x (значение n вводится с клавиатуры, 100<n<999).
 *
 * @author Dmitry
 */


import java.util.Scanner;


public class TaskCh02N031 {

  public static void main(String[] args) {
    int n, c, b, a, z;

    Scanner sc = new Scanner(System.in);
    System.out.println("Задай число = ");
    int n = sc.nextInt();

    if (n >= 100 && n <= 999) {
    c = n / 100 % 10;
    b = n / 10 % 10;
    a = n % 10;
    z = 100 * c + 10 * a + b;
    System.out.println("Начальное число = " + z);
    }
    else System.out.println("Неправильное значение");
  }
}