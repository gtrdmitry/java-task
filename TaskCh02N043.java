﻿/**
 * Даны два целых числа a и b. Если a делится на b или b делится на a, то вывести 1,  
 * иначе — любое другое число. Условные операторы и операторы цикла не использовать.
 *
 * @author Dmitry
 */


import java.util.Scanner;


public class TaskCh02N043 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    System.out.print("Задай a = ");
    int a = in.nextInt();
    System.out.print("Задай b = ");
    int b = in.nextInt();
    System.out.print(a % b * b % a + 1);
  }
}