/**
 * Дан массив. Переписать его элементы в другой массив такого же размера
 * следующим образом: сначала должны идти все отрицательные элементы,
 * а затем все остальные. Использовать только один проход по исходному массиву.
 *
 * @author Dmitry
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TaskCh11N245 {
  public void main() {

    Scanner in = new Scanner(System.in);

    ArrayList<Integer> numbersIn = new ArrayList<>();

    System.out.print("Задай количество элементов в массиве - ");
    int n = in.nextInt();

    for (int i = 0; i < n; i++) {
      System.out.print("Задай " + i + " элемент массива - ");
      numbersIn.add(in.nextInt());
    }

    ArrayList<Integer> numbersOut = new ArrayList<>(numbersIn);
    Collections.sort(numbersOut);

    System.out.println(numbersIn.toString());
    System.out.println(numbersOut.toString());
  }
}